$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();
  

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 800);
});

$(document).ready(function () {
    $(".burger").click(function(){
        $(".drop-menu").toggleClass("active");
    });

    $(".closeMenu").click(function () {
        $(".drop-menu").removeClass("active");
    }); 
    
    $('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
    })
    

    var swiper = new Swiper('.swiper-container', {
        //autoplay: {delay: 2000},
        //loop: true,
        slidesPerView: 'auto',
        slidesPerView: 2,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            0: {
                slidesPerView: 1,
              },
            1200: {
                slidesPerView: 2,
            },
        }
    });
});


var wow = new WOW(
    {
        boxClass: 'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 0,          // distance to the element when triggering the animation (default is 0)
        mobile: true,       // trigger animations on mobile devices (default is true)
        live: true,       // act on asynchronously loaded content (default is true)
        callback: function (box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
    }
);
wow.init();


